import serial
from itertools import takewhile, repeat
from time import sleep
from collections import namedtuple

def read_to_prompt(s):
    return b''.join(takewhile(lambda x: x != b'>', (s.read(1) for i in repeat(None))))

def read_all(s):
    return b''.join(takewhile(bool, (s.read(s.in_waiting) for i in repeat(None))))

BP_SPI_Message = namedtuple('BP_SPI_Message', ['type_', 'value'])

class BP_SPI_MessageType:
    end = b']'
    start = b'['
    byte = b'0'
    overrun = b'C'

def read_spi_byte(s):
    type_ = s.read(1)
    if type_ == b'0':
        data = s.read(9)
        miso = data[1:3]
        mosi = data[6:8]
        value = (miso, mosi)
        print(miso)
    elif type_ == b'C':
        s.read(len(b"ouldn't keep up"))
    else:
        value = None
    return BP_SPI_Message(type_, value)

def read_spi_number(s, cont=None):
    messages = [cont] + list(takewhile(lambda x: x.type_ == BP_SPI_MessageType.byte,
                              (read_spi_byte(s) for i
                              in range(4 - 1 if cont else 0))))
    if not messages:
        return b'', b''
    miso, mosi = zip(*(m.value for m in messages))
    return b''.join(miso), b''.join(mosi)

def read_spi(s):
    while True:
        code = read_spi_byte(s)
        if code.type_ == BP_SPI_MessageType.start:
            print("BEGIN")
        elif code.type_ == BP_SPI_MessageType.end:
            print("EOF")
        elif code.type_ == BP_SPI_MessageType.overrun:
            print("Overrun")
        else:
            miso, mosi = read_spi_number(s, code)
            print("{} {}".format(miso, mosi))
            if len(miso) < 8:
                print("EOF interrupted read")

if __name__ == '__main__':
    s = serial.Serial('/dev/ttyUSB0', baudrate=115200)
    s.write(b'm\n')
    read_to_prompt(s)
    s.write(b'5\n') # SPI
    read_to_prompt(s)
    s.write(b'4\n') # 1MHz
    read_to_prompt(s)
    s.write(b'\n') # default clock polarity
    read_to_prompt(s)
    s.write(b'\n') # default clock edge
    read_to_prompt(s)
    s.write(b'\n') # default input sample
    read_to_prompt(s)
    s.write(b'\n') # default CS
    read_to_prompt(s)
    s.write(b'\n') # default output type
    read_to_prompt(s)
    s.write(b'(1)\n') # sniff
    sleep(1)
    out = read_all(s)
    if not out.endswith(b'Sniffer\r\nAny key to exit\r\n'):
        print("BAD MODE")
        print(out)
    else:
        print(out)
        read_spi(s)
